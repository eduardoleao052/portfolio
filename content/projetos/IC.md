+++
title = 'Iniciação Científica'
date = 2024-10-07T13:11:11-03:00
draft = false
+++

### Digital Twin para a Petrobras

- Nesse projeto, eu ajudei uma equipe de professores e estudantes da Escola Politécnica a desenvolver um sitstema de Perguntas e Respostas (Q&A) sobre um banco de documentos da Petrobras.
- A emprese queria um método de encontrar informações contidas em seus documentos sem ter que varrer todos manualmente. 
- O sistema foi criado como parte do projeto Digital Twin de Plataformas Offshore da Petrobras. Assim, o escopo de documentos para busca é a área de Engenharia Offshore.
- Porém, o treinamento de um sistema de Q&A com modelo de linguagem neural requer um dataset com milhares de perguntas e respostas. Assim, esse projeto teve como objetivo construir um método para gerar perguntas e respostas automaticamente.
- O pipeline de geração de um dataset Q&A usado é o seguinte:

![imgs](/images/criacao.png "pipe")

- O pipeline **SeSO** é composto por um **Retriever**, um **Ranker** e um **Reader**. O Retriever acessa as 11.056 passagens do corpus e seleciona 100 baseados na co-ocorrência de palavras com a pergunta. O Ranker classifica as 10 passagens mais prováveis de conter a resposta para a pergunta, e o Reader deve encontrar a resposta nas 10 passagens dadas.

![imgs](/images/seso.png "seso")

- As perguntas automáticas são usadas como gabarito no treinamento dos modelos Ranker e Reader. Por pergunta, o Ranker atribui a cada passagem uma classificação de 0 a 1: a probabilidade de conter a resposta. Ao receber as perguntas automáticas, o modelo é penalizado por atribuir notas altas para as passagens erradas e baixas para as corretas. Um processo similar ocorre com o Reader, que é penalizado por desviar das respostas automáticas corretas, ao ver as perguntas.