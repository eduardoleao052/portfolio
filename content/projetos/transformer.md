+++
title = 'Transformer'
date = 2024-10-07T13:11:01-03:00
draft = false
+++

### Transformer Generativo

- Nesse projeto, eu tentei recriar um Decoder de arquitetura Transformer, como o Chat GPT.
- O projeto foi desenvolvido com **numpy**, em Python.
- No final, o GPT foi treinado com textos de Shakespeare e Julio Verne, e usado para gerar textos similares.

#### Texto imitando Shakespeare:

<br/>

*LUCIO:*


*Nay, now blame me and my fantasy!*
<br/>

*As thou shalt know now I do love,*
<br/>

*Love the blessed strength of our embrace.*
<br/>
<br/>

*DUKE VINCENTIO:*

*Dark not is thou will here, poor boy!*
<br/>

*What thou hast is a judgment taint,*
<br/>

*And, as much as thou love is real,*
<br/>

*Thou heart wilt shred apart.*
<br/>


*LUCIO:*
<br/>

*Thou rascal! How, my lord, would you rather,*
<br/>

*Conspire on me, betray my friendsip,*
<br/>

*But I shall now bear my own fate.*
<br/>

*I care not, O drunk power: I part with thee,*
<br/>

*I care not, thy firm foe: and he comes not.*


#### Texto imitando Julio Verne:
<div style="text-align: left; margin-left: 10px; margin-bottom: 30px">
<i>Nemo led the frigate by large rocks, the prey which the present
forest of waves marked. But they planted cries surrounded by waters
of prayers and tunnels of the large ocean. Besides, they were going
on to the shore.
The lowest appliances, with peculiar results, hung patterns and
frosts to the bottom, accompanied by the dominion of a strange sound,
was everything that could not be left in this part of the Arctic Circle,
and manufactured at the end of the Rio Norway Island.
The western Norwegian crew was unaccustomed, and the heat of hunger had
their best to remain again. The next danger of twelve miles was from the
Andara, unable to cross the fierce diamond waves with the hollow.
</i>