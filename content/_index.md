
<div class="div_index">
    <h1 style="color: black;">Home</h1>
    Este é o meu portfólio pessoal. Nele, você encontrará:
    <br/>
    <b>1 -</b> Uma <b>página inicial</b>, com biografia e informações de contato.
    <br/>
    <b>2 -</b> O meu <b>Currículo Vitae</b>, com meu histórico acadêmico e atividades extracurriculares.
    <br/>
    <b>3 -</b> Uma página de <b>projetos</b>, com atividades que eu desenvolvi na Escola Politécnica.
    <br/>
    <br/>
    <b>Biografia:</b>
    <br/>
    Sou um estudante do terceiro ano de Engenharia Mecatrônica na Escola Politécnica da Universidade de São Paulo. Tenho interesse em automação e programação, principalmente voltados à aplicações de algoritmos e Inteligência Artificial.
    <br/>
    <br/>
    <b>Informações de Contato:</b>
    <br/>
    Email: eduardoleao052@usp.br
    <br/>
    LinkedIn: <a class="info" href="https://www.linkedin.com/in/eduardoleao052/" target="_blank">eduardoleao052</a>
    <br/>
    GitHub: <a class="info" href="https://github.com/eduardoleao052" target="_blank">eduardoleao052</a>

</div>